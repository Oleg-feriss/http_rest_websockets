import handleUsername from './users/usernames.mjs';
import handleRooms from './rooms/rooms.mjs';
import handleGame from './game/index.mjs';

const username = sessionStorage.getItem('username');
document.querySelector('#username').innerHTML = username;

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

handleUsername(socket);
handleRooms(socket);
handleGame(socket);
