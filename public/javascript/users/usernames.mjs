const resetUsername = () => {
  alert('User with this name already exists! Please enter another name.');
  sessionStorage.clear('username');
  window.location.replace('/login');
};

export default (socket) => {
  socket.on('RESET_USERNAME', resetUsername);
};
