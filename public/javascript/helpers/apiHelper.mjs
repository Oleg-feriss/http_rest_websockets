const API_URL = 'http://localhost:3002';

export async function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .then((result) => result)
    .catch((error) => {
      throw error;
    });
}
