import {
  switchToGame,
  updateRoom,
  leaveRoom,
  setReady,
  setNotready,
  startBeforeGameTimer,
  updateBeforeGameTimer,
  startGame,
  updateGameTimer,
  endGame,
} from './events.mjs';

const backToRoomsBtn = document.querySelector('#back-to-rooms-btn');
const readyBtn = document.querySelector('#game-ready-btn');
const notreadyBtn = document.querySelector('#game-notready-btn');

export default (socket) => {
  readyBtn.addEventListener('click', () => setReady(socket));
  notreadyBtn.addEventListener('click', () => setNotready(socket));
  backToRoomsBtn.addEventListener('click', () => leaveRoom(socket));

  socket.on('UPDATE_ROOM', (room) => updateRoom(socket)(room));
  socket.on('JOIN_ROOM_DONE', (room) => switchToGame(socket)(room));
  socket.on('START_BEFORE_GAME_TIMER', (textIndex) =>
    startBeforeGameTimer(socket)(textIndex)
  );
  socket.on('UPDATE_BEFORE_GAME_TIMER', (seconds) =>
    updateBeforeGameTimer(socket)(seconds)
  );
  socket.on('START_GAME', (roomId) => startGame(socket)(roomId));
  socket.on('UPDATE_GAME_TIMER', (seconds) => updateGameTimer(socket)(seconds));
  socket.on('END_GAME', (winners) => endGame(socket)(winners));
};
