import { createElement, addClass, removeClass } from '../helpers/domHelper.mjs';
import { callApi } from '../helpers/apiHelper.mjs';

const roomsPageElement = document.querySelector('#rooms-page');
const gamePageElement = document.querySelector('#game-page');
const textContainerElement = document.querySelector('#text-container');
const roomNameElement = document.querySelector('#room-name');
const playerListElement = document.querySelector('#game-players');
const backToRoomsBtn = document.querySelector('#back-to-rooms-btn');
const readyBtn = document.querySelector('#game-ready-btn');
const notreadyBtn = document.querySelector('#game-notready-btn');
const beforeGameTimer = document.querySelector('#before-game-timer');
const gameTimer = document.querySelector('#game-timer');

const username = sessionStorage.getItem('username');
let activeRoomId = null;
let textToType = null;

const getText = async (textIndex) => {
  const { text } = await callApi(`/game/texts/${textIndex}`, 'GET');
  textToType = text;
};

const setRoomName = (roomName) => {
  roomNameElement.innerHTML = roomName;
};

const createPlayerProgress = (player) => {
  const playerProgressBar = createElement({
    tagName: 'div',
    className: 'player__progressBar',
  });
  const playerProgressIndicator = createElement({
    tagName: 'div',
    className: 'player__progressIndicator player__progressIndicator-inprocess',
  });

  playerProgressIndicator.style.width = `${player.progress}%`;

  if (player.progress === 100) {
    addClass(playerProgressIndicator, 'player__progressIndicator-done');
    removeClass(playerProgressIndicator, 'player__progressIndicator-inprocess');
  }

  playerProgressBar.append(playerProgressIndicator);
  return playerProgressBar;
};

const createPlayer = (player) => {
  const playerElement = createElement({
    tagName: 'li',
    className: 'game__player player',
  });
  const playerStatus = createElement({
    tagName: 'span',
    className: 'player__status',
  });
  const playerName = createElement({
    tagName: 'span',
    className: 'player__name',
  });
  const playerProgress = createPlayerProgress(player);

  if (player.name === username) {
    addClass(playerElement, 'player-active');
  }

  const statusClass = player.isReady
    ? 'player__status-ready'
    : 'player__status-notready';
  addClass(playerStatus, statusClass);
  playerName.innerHTML = player.name;

  playerElement.append(playerStatus);
  playerElement.append(playerName);
  playerElement.append(playerProgress);

  return playerElement;
};

const setPlayers = (players) => {
  const mapPlayersToElement = players.map(createPlayer);
  playerListElement.innerHTML = '';
  playerListElement.append(...mapPlayersToElement);
};

export const startBeforeGameTimer = (socket) => (textIndex) => {
  addClass(notreadyBtn, 'display-none');
  addClass(backToRoomsBtn, 'display-none');
  removeClass(beforeGameTimer, 'display-none');

  getText(textIndex);
};

export const updateBeforeGameTimer = (socket) => (seconds) => {
  beforeGameTimer.innerHTML = seconds;
};

const emitReady = (socket) => (isReady) => {
  socket.emit('SET_READY', {
    isReady,
    roomId: activeRoomId,
    userId: username,
  });
};

export const setReady = (socket) => {
  addClass(readyBtn, 'display-none');
  removeClass(notreadyBtn, 'display-none');
  emitReady(socket)(true);
};

export const setNotready = (socket) => {
  addClass(notreadyBtn, 'display-none');
  removeClass(readyBtn, 'display-none');
  emitReady(socket)(false);
};

export const leaveRoom = (socket) => {
  socket.emit('LEAVE_ROOM', activeRoomId);
};

export const updateRoom = (socket) => (room) => {
  setRoomName(room.name);
  setPlayers(room.users);
};

export const switchToGame = (socket) => (room) => {
  activeRoomId = room.id;
  addClass(roomsPageElement, 'display-none');
  removeClass(gamePageElement, 'display-none');
  updateRoom(socket)(room);
};

export const startGame = (socket) => (roomId) => {
  addClass(beforeGameTimer, 'display-none');
  removeClass(textContainerElement, 'display-none');
  removeClass(gameTimer, 'display-none');

  const textWithSpan = textToType.replace(/(.)/g, '<span>$1</span>');
  textContainerElement.innerHTML = textWithSpan;

  const textLength = textToType.length;
  let currentCharIndex = 0;
  let currentCharElement = textContainerElement.children[currentCharIndex];
  addClass(currentCharElement, 'char-current');
  let progress;

  const handleKeyUp = (event) => {
    if (event.key === currentCharElement.innerHTML) {
      addClass(currentCharElement, 'char-done');
      removeClass(currentCharElement, 'char-current');
      currentCharIndex = currentCharIndex + 1;
      currentCharElement = textContainerElement.children[currentCharIndex];
      if (currentCharElement) {
        addClass(currentCharElement, 'char-current');
      }
      progress = (currentCharIndex / textLength) * 100;
      socket.emit('SET_PROGRESS', { roomId, userId: username, progress });
    }
  };

  document.addEventListener('keyup', handleKeyUp);
};

export const updateGameTimer = (socket) => (seconds) => {
  gameTimer.innerHTML = `${seconds} seconds left`;
};

export const endGame = (socket) => (winners) => {
  const mapWinners = winners.map((winner, i) => `#${i + 1} ${winner}`);
  alert(mapWinners.join('\n'));
  document.removeEventListener('keyup', () => {});
  textContainerElement.innerHTML = '';
  addClass(textContainerElement, 'display-none');
  addClass(gameTimer, 'display-none');
  removeClass(backToRoomsBtn, 'display-none');
  removeClass(readyBtn, 'display-none');
  addClass(notreadyBtn, 'display-none');
};
