import { createElement, addClass, removeClass } from '../helpers/domHelper.mjs';

const roomsPageElement = document.querySelector('#rooms-page');
const gamePageElement = document.querySelector('#game-page');
const roomsContainer = document.querySelector('#rooms-page-container');

export const updateRooms = (socket) => (rooms) => {
  const createRoomElement = (room) => {
    const roomElement = createElement({
      tagName: 'div',
      className: 'roomCard',
    });
    const roomInfoElement = createElement({
      tagName: 'span',
      className: 'roomCard__info',
    });
    const roomNameElement = createElement({
      tagName: 'h4',
      className: 'roomCard__name',
    });
    const roomBtnElement = createElement({
      tagName: 'button',
      className: 'roomCard__btn btn',
    });

    roomInfoElement.innerHTML = `${room.users.length} users connected`;
    roomNameElement.innerHTML = room.name;
    roomBtnElement.innerHTML = 'Join';

    const onJoinRoom = () => {
      socket.emit('JOIN_ROOM', room.id);
    };

    roomBtnElement.addEventListener('click', onJoinRoom);

    roomElement.append(roomInfoElement);
    roomElement.append(roomNameElement);
    roomElement.append(roomBtnElement);

    return roomElement;
  };

  const roomsElement = rooms.map(createRoomElement);
  roomsContainer.innerHTML = '';
  roomsContainer.append(...roomsElement);
};

export const createRoom = (socket) => () => {
  const roomName = prompt('Enter the name of your room');
  if (roomName) {
    socket.emit('CREATE_ROOM', roomName);
  }
};

export const switchToRooms = () => {
  addClass(gamePageElement, 'display-none');
  removeClass(roomsPageElement, 'display-none');
};

export const occupiedRoom = () => {
  alert('A room with this name already exist! Please enter another room name');
};
