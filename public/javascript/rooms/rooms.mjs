import {
  createRoom,
  updateRooms,
  occupiedRoom,
  switchToRooms,
} from './events.mjs';

const roomCreationBtn = document.querySelector('#room-creating-btn');

export default (socket) => {
  socket.on('UPDATE_ROOMS', (rooms) => updateRooms(socket)(rooms));
  socket.on('OCCUPIED_ROOM', occupiedRoom);
  socket.on('LEAVE_ROOM_DONE', switchToRooms);

  roomCreationBtn.addEventListener('click', () => createRoom(socket)());
};
