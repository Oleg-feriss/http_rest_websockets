import { Server } from 'socket.io';
import * as config from './config';
import handleRooms, { getCurrentRoomId, leaveRoom } from './rooms';

const users = new Set();

export default (io: Server) => {
	io.on("connection", socket => {
    const username = socket.handshake.query.username;

    if (users.has(username)) {
      return socket.emit('RESET_USERNAME');
    }

    users.add(username);

    const disconnecting = () => {
      const roomId = getCurrentRoomId(socket);
      if (roomId) {
        leaveRoom(io, socket)(roomId);
      }
    };

    const disconnect = () => {
      users.delete(username);
    };

    handleRooms(io, socket);

    socket.on('disconnecting', disconnecting);
    socket.on('disconnect', disconnect);
  });
};
