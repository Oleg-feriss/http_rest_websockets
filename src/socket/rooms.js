import * as config from './config';
import { texts } from '../data';

let rooms = [];
const mapGameTimers = new Map();
const mapBeforeGameTimers = new Map();

const isEveryoneReady = (room) => room.users.every((user) => user.isReady);
export const getTextIndex = () => Math.floor(Math.random() * texts.length);

export const getRoomById = (id) => rooms.find((room) => room.id === id);
export const getRoomIndexById = (id) =>
  rooms.findIndex((room) => room.id === id);
export const getRoomByName = (name) => rooms.find((room) => room.name === name);
export const getCurrentRoomId = (socket) =>
  Object.keys(socket.rooms).find((room) => !!getRoomById(room));

export const getUserById = (room, userId) =>
  room.users.find((user) => user.id === userId);
export const getUserIndexById = (room, userId) =>
  room.users.findIndex((user) => user.id === userId);

export const getWinners = (room) => {
  const winners = new Set([
    ...room.winners,
    ...room.users
      .sort((first, second) => second.progress - first.progress)
      .map((user) => user.name),
  ]);

  return Array.from(winners);
};

export const roomIsAvailable = (room) =>
  room.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM &&
  !room.beforeGameTimer &&
  !room.gameTimer;
export const getAvailableRooms = () => rooms.filter(roomIsAvailable);

export const resetRoom = (roomId) => {
  const room = getRoomById(roomId);
  room.winners = [];
  room.beforeGameTimer = false;
  room.gameTimer = false;
  room.users = room.users.map((user) => ({
    ...user,
    progress: 0,
    isReady: false,
  }));
};

export const clearGameTimer = (roomId) => {
  const room = getRoomById(roomId);
  clearInterval(mapGameTimers.get(room.id));
  mapGameTimers.delete(room.id);
  room.gameTimer = false;
};

export const clearBeforeGameTimer = (roomId) => {
  const room = getRoomById(roomId);
  clearInterval(mapBeforeGameTimers.get(room.id));
  mapBeforeGameTimers.delete(room.id);
  room.beforeGameTimer = false;
};

export const deleteRoomById = (roomId) => {
  clearBeforeGameTimer(roomId);
  clearGameTimer(roomId);
  rooms = rooms.filter((room) => room.id !== roomId);
};

export const deleteUserFromRoom = (roomId, userId) => {
  const room = getRoomById(roomId);
  room.users = room.users.filter((user) => user.id !== userId);
};

export const updateEveryOnesRooms = (io) =>
  io.emit('UPDATE_ROOMS', getAvailableRooms());
export const updateActiveRoom = (io) => (roomId) =>
  io.to(roomId).emit('UPDATE_ROOM', getRoomById(roomId));

export const startGame = (io, socket) => (roomId) => {
  const room = getRoomById(roomId);
  const ONE_SECOND_IN_MS = 1000;
  let seconds = config.SECONDS_FOR_GAME;
  let timer = null;

  const updateGameTimer = () => {
    io.to(roomId).emit('UPDATE_GAME_TIMER', seconds);

    if (--seconds < 0) {
      endGame(io, socket)(room);
    }
  };

  io.to(roomId).emit('START_GAME', roomId);

  updateGameTimer();
  timer = setInterval(updateGameTimer, ONE_SECOND_IN_MS);
  room.gameTimer = true;
  mapGameTimers.set(roomId, timer);

  updateEveryOnesRooms(io);
};

export const endGame = (io, socket) => (room) => {
  clearGameTimer(room.id);
  io.to(room.id).emit('END_GAME', getWinners(room));
  resetRoom(room.id);
  updateActiveRoom(io)(room.id);
  updateEveryOnesRooms(io);
};

export const startBeforeGameTimer = (io, socket) => (roomId) => {
  const room = getRoomById(roomId);
  const indexRandomText = getTextIndex();
  const ONE_SECOND_IN_MS = 1000;
  let seconds = config.SECONDS_TIMER_BEFORE_START_GAME;
  let timer = null;

  const updateBeforeGameTimer = () => {
    io.to(roomId).emit('UPDATE_BEFORE_GAME_TIMER', seconds);

    if (--seconds < 0) {
      clearBeforeGameTimer(room.id);
      startGame(io, socket)(roomId);
    }
  };

  io.to(roomId).emit('START_BEFORE_GAME_TIMER', indexRandomText);

  updateBeforeGameTimer();
  timer = setInterval(updateBeforeGameTimer, ONE_SECOND_IN_MS);
  room.beforeGameTimer = true;
  mapBeforeGameTimers.set(roomId, timer);

  updateEveryOnesRooms(io);
};

export const setProgress =
  (io, socket) =>
  ({ roomId, userId, progress }) => {
    const room = getRoomById(roomId);
    const user = getUserById(room, userId);
    user.progress = progress;

    if (progress === 100) {
      room.winners.push(user.name);
    }

    updateActiveRoom(io)(roomId);

    if (room.winners.length === room.users.length) {
      endGame(io, socket)(room);
    }
  };

export const setReady =
  (io, socket) =>
  ({ roomId, userId, isReady }) => {
    const room = getRoomById(roomId);
    const user = getUserById(room, userId);
    user.isReady = isReady;
    updateActiveRoom(io)(roomId);

    if (room && isEveryoneReady(room)) {
      startBeforeGameTimer(io, socket)(roomId);
    }
  };

export const joinRoom = (io, socket) => (roomId) => {
  const { username } = socket.handshake.query;
  socket.join(roomId, () => {
    const activeRoom = getRoomById(roomId);
    const newUser = {
      id: username,
      name: username,
      isReady: false,
      progress: 0,
    };
    activeRoom.users.push(newUser);
    io.to(roomId).emit('JOIN_ROOM_DONE', activeRoom);
    updateEveryOnesRooms(io);
  });
};

export const leaveRoom = (io, socket) => (roomId) => {
  const { username } = socket.handshake.query;
  socket.leave(roomId, () => {
    let leavedRoom = getRoomById(roomId);

    if (leavedRoom) {
      deleteUserFromRoom(roomId, username);
    }

    if (leavedRoom?.users.length <= 0) {
      deleteRoomById(roomId);
      leavedRoom = null;
    }

    socket.emit('LEAVE_ROOM_DONE');
    updateActiveRoom(io)(roomId);
    updateEveryOnesRooms(io);

    if (
      leavedRoom &&
      !leavedRoom.gameTimer &&
      !leavedRoom.beforeGameTimer &&
      isEveryoneReady(leavedRoom)
    ) {
      startBeforeGameTimer(io, socket)(roomId);
    }
  });
};

export const createRoom = (io, socket) => (roomName) => {
  const existingRoom = getRoomByName(roomName);
  if (existingRoom) {
    return socket.emit('OCCUPIED_ROOM');
  }

  const newRoom = {
    id: roomName,
    name: roomName,
    users: [],
    winners: [],
    beforeGameTimer: false,
    gameTimer: false,
  };

  rooms.push(newRoom);

  joinRoom(io, socket)(newRoom.id);
};

export default (io, socket) => {
  if (!getCurrentRoomId(socket)) {
    socket.emit('LEAVE_ROOM_DONE');
  }
  socket.emit('UPDATE_ROOMS', getAvailableRooms());

  socket.on('CREATE_ROOM', (roomName) => createRoom(io, socket)(roomName));
  socket.on('JOIN_ROOM', (roomId) => joinRoom(io, socket)(roomId));
  socket.on('LEAVE_ROOM', (roomId) => leaveRoom(io, socket)(roomId));
  socket.on('SET_READY', (data) => setReady(io, socket)(data));
  socket.on('SET_PROGRESS', (data) => setProgress(io, socket)(data));
};
